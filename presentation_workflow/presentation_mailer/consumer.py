import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()




def process_approval(ch, method, properties, body):
    print("  Received %r" % body)


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)
channel.start_consuming()


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue="process_rejection")
channel.basic_consume(
    queue="process_rejection",
    on_message_callback=process_rejection,
    auto_ack=True,
)
channel.start_consuming()
while True:
    try:
        # ALL OF YOUR CODE THAT HANDLES READING FROM THE
        # QUEUES AND SENDING EMAILS
    
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)